<?php
/*
 * Plugin Name: Rotator
 * Version: 1.0.0
 * Description: Plugin that creates widget, which can have multiple description forms to randomly show in front-end
 */
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
if (!session_id()) {
    session_start();
}
//check for plugin deactivation
register_deactivation_hook(__FILE__, 'on_rotator_deactivation');
register_activation_hook(__FILE__, 'on_rotator_activation');

function rotator_register()
{
    register_widget('Rotator');
}

class Rotator extends WP_Widget
{
    public function __construct(){
        $widget_ops = array('classname' => 'widget_rotator', 'description' => __('Plugin that creates widget, which can have multiple description forms to randomly show in front-end'));
        $control_ops = array('width' => 400, 'height' => 350);
        parent::__construct('rotator', __('Rotator'), $widget_ops, $control_ops);
    }

    public function form($instance)
    {
        $statistsics_url = plugin_dir_url(__FILE__) . "save_statistics.php";
        $widget_id = $this->id;
        ?>
        <script>
            var statistsics_url = '<?php echo $statistsics_url; ?>';
            var widget_id = '<?php echo $widget_id; ?>';
            localStorage.setItem('statistics_url',statistsics_url);
            localStorage.setItem('widget_id',widget_id);
        </script>
        <?php
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = '';
        }
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label></p>
        <div class = "rotator_title_field"><input type="text" class="widefat" name="<?php echo $this->get_field_name('title'); ?>"
                    id="<?php echo $this->get_field_id('title'); ?>" value="<?php echo $title; ?>"></div>
        <br>
        <?php
        if (isset($instance['content'])){
            foreach ($instance['content'] as $key => $content) {
                $title_name = "heading_$key";
                $desc_name = "description_$key";
                $prior_name = "content_priority_$key";
                $disable_name = "disable_rotator_content_$key";
                if (isset($content['disable_rotator_content']))
                    $disabled_or_not = $content['disable_rotator_content'];
                ?>
                <div class="rotator_content_block"
                     id="<?php echo $this->get_field_id("rotator_content_block_$key"); ?>"
                     name="<?php echo $this->get_field_name("rotator_content_block_$key"); ?>">
                    <p>
                    <div class="rotator_toolbar"><a onclick="minimize(jQuery(this))" class="minimize_icon"></a><a onclick = "dummyRemove(jQuery(this))" class="delete_icon"></a></div>
                    </p>
                    <p><label for = "<?php echo $this->get_field_id($title_name); ?>">Type content title</label></p>
                    <p><input type="text" class="widefat" name="<?php echo $this->get_field_name($title_name); ?>"
                              id="<?php echo $this->get_field_id($title_name); ?>"
                              value="<?php if (isset($content['heading'])) echo $content['heading']; ?>"></p>

                    <div class="rotator_minimizable_content">
                        <p><label for = "<?= $this->get_field_id($desc_name) ?>">Type content description</label></p>
                        <p><textarea rows="5" cols="20" class="widefat" name="<?= $this->get_field_name($desc_name) ?>"
                                     id="<?= $this->get_field_id($desc_name) ?>"><?php if (isset($content['description'])) echo $content['description']; ?></textarea>
                        </p>
                        <p><label for = "<?= $this->get_field_id($prior_name) ?>">Select content priority</label></p>
                        <p>
                            <select class="widefat" name="<?= $this->get_field_name($prior_name) ?>"
                                    id="<?= $this->get_field_id($prior_name) ?>">
                                <?php
                                for ($j = 1; $j <= 10; $j++) {
                                    if ($j == $content['priority']) {
                                        echo '<option value = "' . $content['priority'] . '">' . $content['priority'] . '</option>';
                                    }
                                }

                                for ($j = 1; $j <= 10; $j++) {
                                    if ($j != $content['priority']) {
                                        echo '<option value = "' . $j . '">' . $j . '</option>';
                                    }
                                }
                                ?>
                            </select></p>

                        <p><input class="widefat"
                                  type="checkbox" <?php checked((isset($disabled_or_not) && $disabled_or_not) ? $disabled_or_not : 0); ?>
                                  id="<?php echo $this->get_field_id($disable_name); ?>"
                                  name="<?php echo $this->get_field_name($disable_name); ?>">&nbsp;<label
                                for="<?php echo $this->get_field_id($disable_name); ?>"><?php _e('Disable Rotator Content'); ?></label>
                        </p>
                    </div>
                </div>
            <?php
            }
        }

        ?>
        <p><a class="widget-rotator-control-addnew">Add New Content Field</a></p>
    <?php
    }

    public function widget($args, $instance)
    {
        if (!empty($instance['title']))
            $title = $instance['title'];
        else
            $title = 'Rotator plugin';

        echo $args['before_widget'];
        echo $args['before_title'];
        echo $title;
        echo $args['after_title'];
        echo "<div class = \"rotator_widget_content\"></div>";
        echo $args['after_widget'];
        $display_array = array();
        foreach ($instance['content'] as $key => $content) {
            if (isset($content['description']) && !empty($content['description']) && (!isset($content['disable_rotator_content']) or $content['disable_rotator_content'] == '')) {
                $display_array[] = array($content['priority'], $content['description'], $content['heading']);
            }
        }
        ?>
        <script>
            jQuery(document).ready(function () {
                String.prototype.decodeHTML = function () {
                    var map = {"gt": ">" /* , … */};
                    return this.replace(/&(#(?:x[0-9a-f]+|\d+)|[a-z]+);?/gi, function ($0, $1) {
                        if ($1[0] === "#") {
                            return String.fromCharCode($1[1].toLowerCase() === "x" ? parseInt($1.substr(2), 16) : parseInt($1.substr(1), 10));
                        } else {
                            return map.hasOwnProperty($1) ? map[$1] : $0;
                        }
                    });
                };

                var rotator_fields = '<?php echo json_encode($display_array) ; ?>';
                var rotator_fields_object = JSON.parse(rotator_fields);
                console.log(rotator_fields_object);
                if (rotator_fields_object.length > 0) {
                    var rotator_field_test_array = new Array();
                    var rotator_field_title_array = new Array();
                    for (var key in rotator_fields_object) {
                        var priority = parseInt(rotator_fields_object[key][0]);
                        for (var j = 0; j < priority; j++) {
                            rotator_field_test_array.push(rotator_fields_object[key][1]);
                            rotator_field_title_array.push(rotator_fields_object[key][2]);
                        }
                    }
                    var random_number = Math.round(Math.random() * (rotator_field_test_array.length - 1));
                    var decoded = rotator_field_test_array[random_number].decodeHTML().replace(/&lt;/g, '<').replace(/&quot;/g, '"');
                    jQuery('.widget[id*="<?php echo $this->id; ?>"] .rotator_widget_content').append(decoded);
                    var statistics_url = localStorage.getItem('statistics_url');
                    var widget_id = '<?php echo $this->id; ?>';
                    var widget_title = jQuery('.widget[id*="<?php echo $this->id; ?>"] .widget-title').text();
                    jQuery.ajax({
                        "url": statistics_url,
                        "method": "POST",
                        "data": {
                            "action": "save_statistics",
                            "widget_id": widget_id,
                            "content": rotator_field_title_array[random_number],
                            "widget_title":widget_title
                        }
                    }).success(function (data) {
                        console.log(data);
                    }).error(function (message) {
                        console.log(message);
                    });
                }
            });
        </script>
    <?php

    }

    public function update($new_instance, $old_instance){
        $instance_array_keys = array_keys($new_instance);
        $new_instance["content"] = array();
        $widget_id = $this->id;
        global $wpdb;
        foreach ($instance_array_keys as $instance_key){
            if ($instance_key != 'title'){
                if (strpos($instance_key, 'heading') !== false){
                    $timestamp = str_replace('heading_','',$instance_key);
                    $heading = $new_instance[$instance_key];
                    $disable_rotator_content = '';
                    if (isset($new_instance["disable_rotator_content_".$timestamp])){
                        $disable_rotator_content = $new_instance["disable_rotator_content_".$timestamp];
                        unset($new_instance["disable_rotator_content_".$timestamp]);
                    }
                    $new_instance["content"][$timestamp] = array(
                        "heading"=>$heading,
                        "description"=>preg_replace("/\r|\n/", "", esc_textarea($new_instance["description_".$timestamp])),
                        "priority"=>$new_instance["content_priority_".$timestamp],
                        "disable_rotator_content"=>$disable_rotator_content
                    );

                    if ($old_instance[$instance_key] != $new_instance[$instance_key]){
                        $wpdb->update(
                            'rotator_plugin_statistics',
                            array(
                                'content_title' => $new_instance[$instance_key],	// string
                            ),
                            array(
                                'content_title' => $old_instance[$instance_key],
                                'widget_id' => $widget_id
                            ),
                            array('%s'),
                            array('%s', '%s')
                        );
                    }
                    unset($new_instance["description_".$timestamp]);
                    unset($new_instance["heading_".$timestamp]);
                    unset($new_instance["content_priority_".$timestamp]);
                }
            }
        }

        if ($old_instance['title'] != $new_instance['title']){
            global $wpdb;
            $widget_id = $this->id;
            $wpdb->update(
                'rotator_plugin_statistics',
                array(
                    'widget_title' => $new_instance['title'],	// string
                ),
                array(
                    'widget_title' => $old_instance['title'],
                    'widget_id' => $widget_id
                ),
                array('%s'),
                array('%s', '%s')
            );
        }
        return $new_instance;
    }

}

function add_rotator_menu_in_dashboard(){
    $hook = add_dashboard_page('My Plugin Options', 'Add Rotator', 'manage_options', 'add_rotator', 'rotator_plugin_options');
    add_action('load-' . $hook, 'do_on_rotator_plugin_settings_save');
}

function on_rotator_activation(){
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE rotator_plugin_statistics (
  id int(11) NOT NULL AUTO_INCREMENT,
  widget_id varchar(200) NOT NULL,
  widget_title varchar(200) NOT NULL,
  content_title varchar(300) NOT NULL,
  UNIQUE KEY id (id)
) $charset_collate";
    dbDelta($sql);
}

function on_rotator_deactivation(){
    global $wpdb;
    if (!current_user_can('activate_plugins'))
        return;
    $plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
    check_admin_referer("deactivate-plugin_{$plugin}");
    $wpdb->query('DROP TABLE rotator_plugin_statistics');
}

function add_rotator_settings_fields(){
    register_setting('rotator-group', 'rotator_statistics');
}

function rotator_plugin_options(){
    ?>
    <div class="wrap">
        <form action="options.php" method="POST">
            <?php settings_fields('rotator-group'); ?>
            <table class="form-table">
                <tbody>
                <tr class = "statistics"></tr>
                <tr>
                    <td><input name="rotator_statistics" type="checkbox"
                               id="rotator_statistics" <?php checked((get_option('rotator_statistics') == 'on') ? true : 0) ?>><label
                            for="rotator_statistics">Disable statistics</label></td>
                </tr>
                </tbody>
            </table>
            <?php @submit_button(); ?>
        </form>
    </div>
<?php
}

function do_on_rotator_plugin_settings_save(){
    if (isset($_GET['settings-updated']) && $_GET['settings-updated']) {
        $rotator_statistics = get_option('rotator_statistics');
    }
}

function rotator_plugin_stylesheets(){
    wp_enqueue_style('rotator_style', plugin_dir_url(__FILE__) . "/style.css");
    wp_enqueue_script('adminScript', plugin_dir_url(__FILE__) . "/adminScript.js");
    wp_enqueue_script('googleCharJsApi', 'https://www.google.com/jsapi');
    wp_enqueue_script('chartScript', plugin_dir_url(__FILE__) . "/chartScript.js");
}

function load_js(){
    wp_enqueue_script('jquery', 'http://code.jquery.com/jquery-1.11.3.min.js');
    wp_enqueue_style('rotator_front_style', plugin_dir_url(__FILE__) . "/frontStyle.css");
}

function rotator_delete_widget($value){
    if (isset($_POST['delete_widget']) && $_POST['delete_widget'] == 1){
        global $wpdb;
        $wpdb->delete( 'rotator_plugin_statistics', array( 'widget_id' => $_POST['the-widget-id'] ), array( '%s' ) );
    }
    return $value;
}

add_action('widgets_init', 'rotator_register');
add_action('admin_enqueue_scripts', 'rotator_plugin_stylesheets');
add_action('wp_enqueue_scripts', 'load_js');
add_action('admin_menu', 'add_rotator_menu_in_dashboard');
add_action('admin_init', 'add_rotator_settings_fields');
add_action('pre_update_option_sidebars_widgets', 'rotator_delete_widget');