/**
 * Created by Hrayr on 15.07.2015.
 */
function dummyRemove(object, e){
    var index = jQuery('.delete_icon').index(object);
    jQuery('.rotator_content_block').eq(index).remove();
}

function minimize(object){
    var index = jQuery('.minimize_icon').index(object);
    if (jQuery('.minimize_icon').eq(index).hasClass('minizmized')){
        jQuery('.rotator_minimizable_content').eq(index).slideDown();
        jQuery('.minimize_icon').eq(index).addClass('maxizmized');
        jQuery('.minimize_icon').eq(index).removeClass('minizmized');
        mimimize_clicked = 0;
    }
    else{
        jQuery('.rotator_minimizable_content').eq(index).slideUp();
        jQuery('.minimize_icon').eq(index).addClass('minizmized');
        jQuery('.minimize_icon').eq(index).removeClass('maxizmized');
    }
}


jQuery(document).ready(function () {

    jQuery('#widgets-right').on("click", '.widget-rotator-control-addnew', function (event) {
        var new_desc_template = '<div class = "rotator_content_block" id = "rotator_content_block_" name = "rotator_content_block_">';
        new_desc_template += '<p><div class="rotator_toolbar"><a onclick = "minimize(jQuery(this))" class = "minimize_icon"></a><a class = "delete_icon" onclick = "dummyRemove(jQuery(this))"></a></div></p><p><label for = "heading_" class = "widefat">Type content title</label></p><p><input type = "text" class = "widefat" name = "heading_" id = "heading_"></p>';
        new_desc_template += '<div class = "rotator_minimizable_content"><p><label for = "description_" class = "widefat">Type content description</label></p><p><textarea rows = "5" cols = "20" class = "widefat" name = "description_" id = "description_"></textarea></p>';
        new_desc_template += '<p><label for = "content_priority_" class = "widefat">Select content priority</label></p><p><select class = "widefat" name = "content_priority_" id = "content_priority_">';
        for (var ti = 0;ti<10;ti++){
            new_desc_template += '<option value = "' + (ti+1) +  '">'+ (ti+1) +'</option>';
        }
        new_desc_template += '</select></p></div></div>';
        var title_field = jQuery(event.target).parent().find('.rotator_title_field');
        var widget_number = parseInt(jQuery(title_field).children()[0].id.split("-")[2]);
        var widget_name = 'widget-rotator';
        var desc_field_count = jQuery(event.target).parent().find('.rotator_content_block').length;
        if (desc_field_count == 0) {
            jQuery(title_field).after(new_desc_template);
        }
        else {
            jQuery(event.target).parent().find('.rotator_content_block').last().after(new_desc_template);
        }

        var all_fields = jQuery(event.target).parent().find('.rotator_content_block').last().find('.widefat');
        all_fields.push(jQuery(event.target).parent().find('.rotator_content_block').last()[0]);
        timestamp = new Date().getTime();
        //iterate over all input fields
        for (var i = 0; i < all_fields.length; i++) {
            object = all_fields[i];
            if (object.tagName == 'LABEL'){
                object_for = object.getAttribute('for');
                object.setAttribute("for", widget_name +  '-' + widget_number +  '-' + object_for + timestamp);
            }
            else{
                object_id = object.getAttribute('id');
                object_name = object.getAttribute('name');
                object.setAttribute("id", widget_name +  '-' + widget_number +  '-' + object_id + timestamp);
                object.setAttribute("name",widget_name +  '[' + widget_number +  '][' + object_name + timestamp + ']');
            }

        }
    });

});