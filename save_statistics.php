<?php
//mimic the actual admin-ajax
define('DOING_AJAX', true);

ini_set('html_errors', 0);
define('SHORTINIT', true);
require_once('../../../wp-load.php');
session_start();


if (isset($_POST['action'])){
    global $wpdb;
    $rotator_statistics = get_option('rotator_statistics');
    if (isset($rotator_statistics) && $rotator_statistics != 'on') {
        if ($_POST['action'] == 'save_statistics' && isset($_POST['widget_id'],$_POST['content'], $_POST['widget_title'])) {
            echo 'Inserting';
                $wpdb->insert(
                    'rotator_plugin_statistics',
                    array(
                        'widget_id' => $_POST['widget_id'],
                        'content_title' => $_POST['content'],
                        'widget_title'=>ucfirst($_POST['widget_title'])
                    ),
                    array(
                        '%s',
                        '%s',
                        '%s'
                    )
                );
        }
        else if ($_POST['action'] == 'get_statistics' && isset($_POST['widget_id'])) {
            $result = $wpdb->get_results("SELECT content_title, COUNT(*), widget_id, widget_title FROM rotator_plugin_statistics WHERE widget_id = '{$_POST['widget_id']}' GROUP BY content_title ORDER BY COUNT(*)", ARRAY_N);
            echo json_encode($result);
        }
        else if ($_POST['action'] == 'get_widget_ids'){
            $result = $wpdb->get_results("SELECT DISTINCT widget_id FROM rotator_plugin_statistics", ARRAY_N);
            echo json_encode($result);
        }
    }
    if ($_POST['action'] == 'delete_statistics' && isset($_POST['content'], $_POST['widget_id'])){
        echo 'deleting';
        $wpdb->delete( 'rotator_plugin_statistics', array( 'content_title' => $_POST['content'], 'widget_id' => $_POST['widget_id'] ), array( '%s', '%s' ) );
    }

}
else{
    die('-1');
}