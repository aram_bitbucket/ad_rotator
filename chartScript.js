/**
 * Created by Hrayr on 21.07.2015.
 */
google.load('visualization', '1.1', {'packages':['bar']});

// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawChart);

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart() {
    //get all widget-ids
    var widget_ids = new Array();
    var statistics_url = localStorage.getItem('statistics_url');
    jQuery.ajax({
        "url":statistics_url,
        "method":"POST",
        "data":{
            "action":"get_widget_ids"
        }
    }).success(function(data){
        widget_ids = JSON.parse(data);
        console.log(widget_ids);
        for (i = 0;i<widget_ids.length;i++){
            widget_id = widget_ids[i][0];
            jQuery.ajax({
                "url":statistics_url,
                "method":"POST",
                "data":{
                    "action":"get_statistics",
                    "widget_id":widget_id
                }
            }).success(function(info){
                json_data = JSON.parse(info);
                console.log(json_data);
                if (json_data.length>0){
                    var local_widget_id = json_data[0][2];
                    template = '<tr><td><div id = "chart_div_' + local_widget_id +'"></div></td></tr>';
                    container_id = 'chart_div_' + local_widget_id;
                    jQuery('.statistics').append(template);
                    rotator_statistics = new Array();
                    header_columns = new Array('');
                    bar_colors = new Array();
                    body_columns = new Array('Titles');
                    for (var j = 0;j<json_data.length;j++){
                        header_columns.push(json_data[j][0]);
                        body_columns.push(json_data[j][1]);
                        bar_colors.push(getRandomColor());
                    }
                    rotator_statistics.push(header_columns);
                    rotator_statistics.push(body_columns);
                    var data = google.visualization.arrayToDataTable(rotator_statistics);
                    console.log(bar_colors);
                    var options = {
                        chart: {
                            title: json_data[0][3]
                        },
                        bars: 'vertical',
                        width:900,
                        height: 200,
                        colors: ['#F21B2D', '#1DF2DD', '#0785F2', '#13F210', '#F2E81B'],
                        hAxis:{
                            position:'none'
                        }
                    };

                    var chart = new google.charts.Bar(document.getElementById(container_id));

                    chart.draw(data, google.charts.Bar.convertOptions(options));
                }
            }).error(function(message){
                console.log(message);
            });
        }
    }).error(function(message){
        console.log(message);
    });

}
